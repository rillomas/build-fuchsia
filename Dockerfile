FROM ubuntu

RUN apt-get update
RUN apt-get install -y golang git build-essential curl unzip python
WORKDIR /work

RUN curl -s https://raw.githubusercontent.com/fuchsia-mirror/jiri/master/scripts/bootstrap_jiri | bash -s fuchsia
ENV PATH $PATH:/work/fuchsia/.jiri_root/bin
RUN cd /work/fuchsia && jiri import fuchsia https://fuchsia.googlesource.com/manifest && jiri update
RUN /bin/bash -c "source /work/fuchsia/scripts/env.sh; fset x86-64; fbuild;"
ENTRYPOINT ["/bin/bash","-c","source /work/fuchsia/scripts/env.sh; fset x86-64; frun;"]
